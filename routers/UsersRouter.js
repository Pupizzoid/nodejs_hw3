const { Router } = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const { handleGetUsers, handlePatchUsers, handleDeleteUsers } = require('../controllers/UsersController');

router.get('/users/me', authMiddleWare, handleGetUsers);
router.patch('/users/me/password', authMiddleWare, handlePatchUsers);
router.delete('/users/me', authMiddleWare, handleDeleteUsers);

module.exports = router;
