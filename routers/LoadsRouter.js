const { Router } = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const {
  handleGetLoads,
  handleCreateLoads,
  handleGetActiveLoads,
  handleUpdateActiveLoads,
  handleGetLoadById,
  handleUpdateLoadById,
  handleDeleteLoadById,
  handlePostLoad,
  handleGetActiveLoadById,
} = require('../controllers/LoadsController');

router.get('/loads', authMiddleWare, handleGetLoads);
router.post('/loads', authMiddleWare, handleCreateLoads);
router.get('/loads/active', authMiddleWare, handleGetActiveLoads);
router.patch('/loads/active/state', authMiddleWare, handleUpdateActiveLoads);
router.get('/loads/:id', authMiddleWare, handleGetLoadById);
router.put('/loads/:id', authMiddleWare, handleUpdateLoadById);
router.delete('/loads/:id', authMiddleWare, handleDeleteLoadById);
router.post('/loads/:id/post', authMiddleWare, handlePostLoad);
router.get('/loads/:id/shipping_info', authMiddleWare, handleGetActiveLoadById);

module.exports = router;
