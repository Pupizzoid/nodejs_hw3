const { Router } = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const {
  handleGetTrucks,
  handleCreateTruck,
  handleGetTruckById,
  handleUpdateTruckById,
  handleDeleteTruckById,
  handleAssignTruckByUser,
} = require('../controllers/TrucksControllers');

router.get('/trucks', authMiddleWare, handleGetTrucks);
router.post('/trucks', authMiddleWare, handleCreateTruck);
router.get('/trucks/:id', authMiddleWare, handleGetTruckById);
router.put('/trucks/:id', authMiddleWare, handleUpdateTruckById);
router.delete('/trucks/:id', authMiddleWare, handleDeleteTruckById);
router.post('/trucks/:id/assign', authMiddleWare, handleAssignTruckByUser);

module.exports = router;
