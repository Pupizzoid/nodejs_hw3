/* eslint-disable camelcase */
const User = require('../models/User');
const Truck = require('../models/Truck');
const Load = require('../models/Load');

const abstract = {
  findUserByEmail: (email) => {
    return User.findOne({ email });
  },
  createUser: (data) => {
    const created_date = new Date().toLocaleString();
    const { email } = data;
    return new User({ ...data, created_date, email: email.toLowerCase() }).save();
  },
  findUserById: (id, callback) => {
    return User.findById(id, callback);
  },
  deleteUserById: (id, callback) => {
    return User.findOneAndRemove({ _id: id }, callback);
  },
  changeUserPassword: (id, password, callback) => {
    return User.findByIdAndUpdate(id, password, callback);
  },
  findUsersTrucks: (id, callback) => {
    return Truck.find({ created_by: id }, callback);
  },
  findAssignedUserTruck: (id, callback) => {
    return Truck.find({ assigned_to: id }, callback);
  },
  createTruck: (type, id) => {
    const created_date = new Date().toLocaleString();
    return new Truck({ type, created_date, status: 'OS', assigned_to: 'null', created_by: id }).save();
  },
  findTruck: (param, callback) => {
    return Truck.find({ $or: [...param] }, callback);
  },
  findTruckById: (id, callback) => {
    return Truck.findById(id, callback);
  },
  updateTruckById: (id, params, callback) => {
    return Truck.findByIdAndUpdate(id, params, callback);
  },
  deleteTruckById: (id, callback) => {
    return Truck.findByIdAndRemove(id, callback);
  },
  findLoads: (params, callback) => {
    return Load.find(params, callback);
  },
  findLoadsForDriver: (id, callback) => {
    return Load.find({ $or: [{ assigned_to: id, status: 'ASSIGNED' }, { assigned_to: id, status: 'SHIPPED' }] }, callback);
  },
  createLoad: (data) => {
    const created_date = new Date().toLocaleString();
    return new Load({ ...data, created_date, assigned_to: 'null', status: 'NEW' }).save();
  },
  findActiveLoadForDriver: (id, callback) => {
    return Load.find({ assigned_to: id, status: 'ASSIGNED' }, callback);
  },
  findLoadById: (id, callback) => {
    return Load.findById(id, callback);
  },
  deleteLoadById: (id, callback) => {
    return Load.findByIdAndRemove(id, callback);
  },
  updateLoadByParams: (id, params, callback) => {
    return Load.findByIdAndUpdate(id, params, callback);
  },
};

module.exports = abstract;
