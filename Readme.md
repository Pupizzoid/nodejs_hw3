# Simple Uber Application

In this App you can create your account and create trucks and loads(these actions dependent on your role). 
Server was created with express.js, UI part with React.

## Installation
#### 1. Install node.js

#### 2. Clone repository and install dependency

## Available Scripts

#### In the project directory, you can run:

##### This script to run a server.

```bash
npm start
```
##### This script to run a server and frontend part.

```bash
npm run dev
```