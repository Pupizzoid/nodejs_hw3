import React, { useState} from 'react';
import { UserRoutes } from './routes';
import AppContext from './context/AppContext';
import { BrowserRouter} from 'react-router-dom';
import {
	Box,
} from '@material-ui/core';


const App = () => {
	const [infoMessage, setInfoMessage] = useState('');
	const value = {
		infoMessage,
		setInfoMessage
	};

	return (
		<AppContext.Provider value={value}>
			<BrowserRouter>
				<Box>
					<UserRoutes/>
				</Box>
			</BrowserRouter>
			</AppContext.Provider>
  );
}

export default App;
