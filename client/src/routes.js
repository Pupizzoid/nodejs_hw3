import React from 'react';
import { Switch, Route, Redirect }from 'react-router-dom';
import AuthPage from './components/AuthPage';
import RegisterPage from './components/RegisterPage';
import Account from './components/Account';

export const UserRoutes = () => {
	return (
		<Switch>
			<Route path='/' component={AuthPage} exact />
			<Route path="/register" component={RegisterPage} exact />
			<Route path="/account" component={Account} />
			<Redirect to='/'/>
		</Switch>
	)
}
