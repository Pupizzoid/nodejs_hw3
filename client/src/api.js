import { http } from './http.js';

const api = {
	register: (user) => {
		return http.post(`/api/auth/register`, user, false);
	},
	login: (user) => {
		return http.post(`/api/auth/login`, user, false );
	},
	getUser: () => {
		return http.get(`/api/users/me`);
	},
	patchUser: (passwords) => {
		return http.patch(`/api/users/me/password`, passwords);
	},
	deleteUser: () => {
		return http.delete(`/api/users/me`);
	},
	getTrucks: () => {
		return http.get(`/api/trucks`);
	},
	postTruck: (type) => {
		return http.post(`/api/trucks`, type);
	},
	deleteTruck: (id) => {
		return http.delete(`/api/trucks/${id}`);
	},
	putTruck: (id, type) => {
		return http.put(`/api/trucks/${id}`, type);
	},
	getTruck: (id) => {
		return http.get(`/api/trucks/${id}`);
	},
	assignTruck: (id) => {
		return http.post(`/api/trucks/${id}/assign`);
	},
	getLoads: (params) => {
		if (params) {
			return http.get(`/api/loads/?${params}`);
		}
		return http.get(`/api/loads`);
	},
	postLoads: (load) => {
		return http.post(`/api/loads`, load);
	},
	getActiveLoads: () => {
		return http.get(`/api/loads/active`);
	},
	patchLoad: () => {
		return http.patch(`/api/loads/active/state`);
	},
	getLoad: (id) => {
		return http.get(`/api/loads/${id}`);
	},
	putLoad: (id, load) => {
		return http.put(`/api/loads/${id}`, load);
	},
	deleteLoad: (id) => {
		return http.delete(`/api/loads/${id}`);
	},
	assignLoads: (id) => {
		return http.post(`/api/loads/${id}/post`);
	},
	getShippingInfo: (id) => {
		return http.get(`/api/loads/${id}/shipping_info`);
	},
};

export default api;
