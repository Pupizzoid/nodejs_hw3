export const load = {
	name: '',
	payload: '',
	pickup_address: '',
	delivery_address: '',
	dimensions: {
		width: '',
		length: '',
		height: ''
	},
	logs: []
}

export const shipInfo = {
	status: '',
	type: ''
}


export const resetMessage = (message, func) => {
	const reset = func('');
	return setTimeout(reset, 1000);
}
