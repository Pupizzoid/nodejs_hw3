import React, { useState, useEffect, useContext, Fragment } from 'react';
import { Link } from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';
import api from '../api';
import useStyles from './ListStyle';
import InfoMessage from './InfoMessage';
import AppContext from '../context/AppContext';
import { load } from '../utils';
import {
	Box,
	ListItem,
	Typography,
	Button,
	List,
	Card,
	CardContent,
	Icon,
	ButtonGroup,
	Fab
} from '@material-ui/core';


const LoadsList = () => {
	const classes = useStyles();
	const [dataLoads, setDataLoads] = useState([]);
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage, userData } = context;
	const [ formAction, setFormAction ] = useState('create');
	const [formTitle, setFormTitle] = useState('Create Load');
	const [loadData, setData] = useState(load);
	context.loadState = loadData;
	context.setLoadState = setData;
	context.formAction = formAction;
	context.setFormAction = setFormAction;
	context.formTitle = formTitle;
	context.setFormTitle = setFormTitle;
	const reset = () => {
		setInfoMessage('');
	}

	useEffect(() => {
		try {
			api.getLoads()
				.then((...data) => {
					if (data.message) {
						setInfoMessage(data.message);
						setInterval(reset, 1000)
					}
					const newData = data[0].loads;
					setDataLoads([...newData]);
			})
		} catch (e) {
		}
	}, []);

	const handleDeleteLoad = (id) => {
		api.deleteLoad(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000)
				}
		})
	}

	const handleAllLoad = () => {
		api.getLoads()
			.then((...data) => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000)
				}
				const newData = data[0].loads;
				setDataLoads([...newData]);
			})
	}


	const handleFilteredLoad = (filter) => {
		api.getLoads(filter)
			.then((...data) => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000)
				}
				const newData = data[0].loads;
				setDataLoads([...newData]);
			})
	}

	const drawLoads = dataLoads.map(load => {
		return (
			<ListItem key={load._id}>
				<Card className={classes.card}>
					<CardContent className={classes.content}>
					<Typography
						color="textSecondary"
						component="p"
						className={classes.text}>
						Name: {load.name}
						</Typography>
						<Typography
							color="textSecondary"
							component="p"
							className={classes.text}>
							Payload: {load.payload}
						</Typography>
						<Typography
							color="textSecondary"
							component="p"
							className={classes.text}>
							Status: {load.status}
						</Typography>
						<Box
							className={`${classes.buttonWrapper} + ${classes.buttonContainer}`}>
							{userData.role !== 'DRIVER' ?
								<Button>
								<Icon
									color="primary"
									onClick={() => handleDeleteLoad(load._id)}
									className={classes.icon}>
									delete
								</Icon>
								</Button> :
								null
							}
							<Link to={`/account/loads/${load._id}`}>
							<Icon
									color="primary"
								className={classes.icon}
							>
									visibility
								</Icon>
							</Link>
						</Box>
					</CardContent>
					</Card>
			</ListItem>
		)
	});

	return (
		<Box className={classes.root}>
				<Fragment>
					<Box className={classes.header}>
						<Typography
							color="textSecondary"
							component="h2"
							className={classes.title}>
						Your Loads
							{userData.role === 'SHIPPER' ?
								<Box>
									<Link
										color="primary"
										className={classes.fab}
										to='/account/form'
									>
										<AddIcon />
									</Link>
							</Box> :
							null
							}
						</Typography>
					{userData.role === 'SHIPPER' ?
						<ButtonGroup
							variant="contained"
							color="primary"
							aria-label="contained primary button group"
						>
							<Button onClick={handleAllLoad}>All</Button>
							<Button onClick={() => handleFilteredLoad(`status=assigned`)}>Assigned</Button>
							<Button onClick={() => handleFilteredLoad(`status=posted`)}>Posted</Button>
							<Button onClick={() => handleFilteredLoad(`status=new`)}>New</Button>
							<Button onClick={() => handleFilteredLoad(`status=shipped`)}>Shipped</Button>
						</ButtonGroup> :
							null
							}
					</Box>
					<Box className={classes.content}>
						{drawLoads.length < 1 ?
							<Typography
								color="textSecondary"
								component="h4"
								className={classes.subTitle}>
								You don't have any loads!
							</Typography> :
							<List className={classes.list}>
								{drawLoads}
							</List>
						}
					</Box>
				</Fragment>
				{infoMessage ?
					<InfoMessage info={infoMessage} /> :
					null
						}
			</Box>
		)
}

export default LoadsList;