import React, {useEffect, useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, Link, Route } from 'react-router-dom';
import Profile from './Profile';
import Truck from './Truck';
import api from '../api';
import TrucksList from './TrucksList';
import LoadsList from './LoadsList';
import InfoMessage from './InfoMessage';
import AppContext from '../context/AppContext';
import Load from './Load';
import {
	Box,
	AppBar,
	Toolbar,
	Typography,
	Button
} from '@material-ui/core';
import Form from './Form';

const useStyles = makeStyles((theme) => ({
	link: {
		color: 'white',
		marginRight: 20,
	},
	nav: {
		display: 'flex',
		justifyContent: 'space-around'
	}
}));

const Account = () => {
	const classes = useStyles();
	const history = useHistory();
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage } = context
	const [userData, setUserData] = useState({
		email: '',
		role: ''
	})
	context.userData = userData;
	context.setUserData = setUserData;

	const reset = () => {
		setInfoMessage('');
	}

	const handleLogOut = () => {
		localStorage.removeItem('token');
		history.push('/');
	}

	useEffect(() => {
		try {
			api.getUser()
				.then(data => {
					if (data.message) {
						setInfoMessage(data.message);
						setTimeout(reset, 3000);
					}
					setUserData({
						...userData,
						email: data.user.email,
						role: data.user.role,
					});
			})
		} catch (e) {
		}
	}, []);


	return (
		<Box>
				<AppBar position="static" >
					<Toolbar className={classes.nav}>
						{userData.role === 'DRIVER' ?
							<Box>
								<Link to='/account/trucks' className={classes.link}>Your Trucks</Link>
								<Link to='/account/loads' className={classes.link}>Your Loads</Link>
								<Link to='/account/profile' className={classes.link}>Profile</Link>
							</Box>
							:
							<Box>
								<Link to='/account/loads' className={classes.link}>Your Loads</Link>
								<Link to='/account/profile' className={classes.link}>Profile</Link>
							</Box>
						}
					<Typography variant="h6" className={classes.title}>
            Hello
          </Typography>
						<Button
							color="secondary"
							variant='contained'
							onClick={handleLogOut}
						>
							Logout
						</Button>
					</Toolbar>
			</AppBar>
			<Route path="/account/trucks" component={TrucksList} exact />
				<Route path="/account/trucks/:id" component={Truck} exact />
				<Route path="/account/loads/:id" component={Load} exact />
			<Route path="/account/profile" component={Profile} exact /> 
			<Route path="/account/loads" component={LoadsList} exact /> 
			<Route path="/account/form" component={Form} exact /> 
			<Route path='/account/history' className={classes.link}>Profile</Route>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
				null
		}
			</Box>
	)
}

export default Account;