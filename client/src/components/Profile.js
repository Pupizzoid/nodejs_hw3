import React, { useState, useEffect, useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InfoMessage from './InfoMessage';
import AppContext from '../context/AppContext';
import { useHistory } from 'react-router-dom';
import api from '../api';
import {
	Typography,
	Button,
	Box,
	TextField
} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
		margin: 'auto',
		width: '50%',
		display: 'flex',
		flexWrap: 'wrap',
		margin: 'auto',
		justifyContent: 'center',

  },
	btnContainer: {
		width: 'fit-content',
	},
	boxInfo: {
		marginBottom: 30,
		width: '100%',
		textAlign: 'center'
	},
	form: {
		width: '30%',
		display: 'flex',
		justifyContent: 'space-between'
	},
	field: {
		flexGrow: 1,
		paddingRight: 15,
	},
	button: {
		marginRight: 20
	},
	info: {
		fontSize: 20,
		marginTop: 30
	},
	title: {
		fontSize: 24
	}
});

const Profile = () => {

	const { infoMessage, setInfoMessage } = useState('')
	const [isEditPasswordState, setEditPasswordState] = useState(false);
	const { userData } = useContext(AppContext);
	const [passwords, setPasswords] = useState({
		oldPassword: '',
		newPassword: ''
	});
	const history = useHistory();

	const handleDeleteUserAccount = () => {
		api.deleteUser();
		localStorage.removeItem('token');
		history.push('/');
	}

	const handleChangePassword = () => {
		setEditPasswordState(true);
	}

	const handleSubmitNewPassword = (e) => {
		e.preventDefault();
		const { newPassword, oldPassword } = passwords;
		if (newPassword === '' || oldPassword === '') {
			return;
		}
		api.patchUser(passwords)
			.then(data => {
				if (data.message) {
					const message = data.message;
					setInfoMessage(message);
				}
		});
		setEditPasswordState(false);
		setPasswords({
			oldPassword: '',
			newPassword: ''
		});
	}

	const handleCloseForm = () => {
		setEditPasswordState(false);
	}

	const handleChangePasswordInput = (e) => {
		setPasswords({ ...passwords, [e.target.name]: e.target.value.trim() });
	}

	const classes = useStyles();
	return (
		<Box className={classes.root}>
			<Box className={classes.boxInfo}>
			<Typography
					color="textSecondary"
					className={classes.title}
						component="h1">
					Your Profile
				</Typography>
				<Typography
					color="textSecondary"
					className={classes.info}
					component="p">
					Your email: {userData.email}
				</Typography>
				<Typography
					color="textSecondary"
					className={classes.info}
					component="p">
					Your role: {userData.role}
				</Typography>
				{infoMessage ?
					<InfoMessage info={infoMessage} /> :
					null
				}
				{isEditPasswordState ?
					<form
						className={classes.form}
						onSubmit={handleSubmitNewPassword}>
						<TextField
							label="Old password"
							name='oldPassword'
							value={passwords.oldPassword}
							className={classes.field}
							onChange={handleChangePasswordInput}
							/>
						<TextField
							label="New password"
							name='newPassword'
							value={passwords.newPassword}
							className={classes.field}
							onChange={handleChangePasswordInput}
						/>
						<Button
							variant="contained"
							color="primary"
							type='submit'
							className={classes.button}
						>
							Save
						</Button>
						<Button
							variant="contained"
							color="primary"
							type='submit'
							className={classes.button}
							onClick={handleCloseForm}
						>
							Cancel
						</Button>
					</form> :
					null
				}
			</Box>
			<Box className={classes.btnContainer}>
				<Button
					onClick={handleChangePassword}
					variant='contained'
					color='primary'
					className={classes.button}
				>
					Change Password
				</Button>
				<Button
					onClick={handleDeleteUserAccount}
					variant='contained'
					color='primary'
					className={classes.button}
				>
					Delete Account
				</Button>
			</Box>
		</Box>
	)
}

export default Profile;