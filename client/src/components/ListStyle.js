import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
		width: '100%',
		display: 'flex',
		flexWrap: 'wrap'
	},
	header: {
		width: '50%',
		display: 'flex',
		justifyContent: 'center',
		flexWrap: 'wrap',
		alignContent: 'center',
		marginBottom: 30,
		margin: 'auto'
	},
	form: {
		width: '100%',
	},
	title: {
		width: '100%',
		fontSize: 36,
		textAlign: 'center',
		marginBottom: 30,
		display: 'block'
	},
	content: {
		width: '100%',
		textAlign: 'center'
	},
	button: {
		display: 'block',
		margin: 'auto',
	},
	form: {
		width: '50%',
		marginBottom: 30,
		display: 'flex',
		justifyContent: 'space-around',
		alignContent: 'center',
		flexWrap: 'wrap',
		margin: 'auto',
	},
	field: {
		width: '60%',
		marginBottom: 30,
	},
	buttonContainer: {
		width: '50%',
		display: 'flex',
		justifyContent: 'space-around',
		alignItems: 'center',
	},
	buttonWrapper: {
		width: '20%'
	},
	list: {
		width: '50%',
		margin: 'auto',
	},
	card: {
		width: '100%'
	},
	content: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-around',
	},
	fab: {
		marginLeft: 40,
		color: 'white',
		backgroundColor: '#3f51b5',
		'&:hover': {
			backgroundColor: '#303f9f'
		},
		borderRadius: '50%',
		display: 'inline-block',
		width: 56, 
		height: 56,
		boxShadow: '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12);',
		transition: 'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;'
	}
});

export default useStyles;