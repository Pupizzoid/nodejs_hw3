import React, { useState, useContext } from 'react';
import api from '../api';
import { useHistory } from 'react-router-dom';
import InfoMessage from './InfoMessage';
import authStyle from './AuthStyle';
import AppContext from '../context/AppContext';
import {
	CardContent,
	Card,
	Typography,
	Button,
	TextField,
	Box,
	Radio,
	RadioGroup,
	FormControlLabel,
	FormControl,
	FormLabel
} from '@material-ui/core';


const RegisterPage = () => {
	const classes = authStyle();
	const history = useHistory();
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage, } = context;
	const [registerForm, setRegisterForm] = useState({
		password: '',
		email: '',
		role: 'SHIPPER'
	});
	const reset = () => {
		setInfoMessage('');
	}

	const handlerChangeInput = (e) => {
		setRegisterForm({
			...registerForm,
			[e.target.name]: e.target.value 
		})
	}

	const handleSubmit = (e) => {
		e.preventDefault();
		api.register(registerForm)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message)
					setTimeout(reset, 3000);
				}
			});
		history.push('/');
	}

	return (
		<Box>
			<Card className={classes.authCard}>
				<CardContent>
					<Typography
						color='textSecondary'
						component='h1'
						className={classes.title}>
						Registration
					</Typography>
					<form
						className={classes.formAuth}
						onSubmit={handleSubmit}
					>
						<TextField
							id='email'
							label='Email'
							variant='outlined'
							name='email'
							className={classes.field}
							value={registerForm.email}
							onChange={handlerChangeInput}
						/>
						<TextField
							id='password'
							label='Password'
							variant='outlined'
							placeholder='******'
							name='password'
							className={classes.field}
							value={registerForm.password}
							onChange={handlerChangeInput}
						/>
						<FormControl component="fieldset">
							<FormLabel component="legend">Your Role</FormLabel>
							<RadioGroup aria-label="gender" name="role" value={registerForm.role} onChange={handlerChangeInput}>
								<FormControlLabel value="SHIPPER" control={<Radio />} label="SHIPPER" />
								<FormControlLabel value="DRIVER" control={<Radio />} label="DRIVER" />
							</RadioGroup>
						</FormControl>
						<Button
							variant='contained'
							color='primary'
							className={classes.button}
							type='submit'
							// onClick={handleSubmitLoginData}
						>
							Sign up
						</Button>
					</form>
				</CardContent>
			</Card>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
			null
		}
		</Box>
	)
}

export default RegisterPage;
