import React, { useState, useEffect, useContext, Fragment } from 'react';
import { useParams, Link}from 'react-router-dom';
import api from '../api';
import useStyles from './CardStyle';
import InfoMessage from './InfoMessage';
import { load as loadData, shipInfo } from '../utils';
import AppContext from '../context/AppContext';
import {
	Typography,
	Button,
	Box,
	Card,
	CardContent,
	Accordion, 
	AccordionDetails,
	AccordionSummary,
	Icon,
} from '@material-ui/core';


const Load = () => {
	const classes = useStyles();
	const { id } = useParams();
	const [load, setLoad] = useState(loadData);
	const [info, setInfo] = useState(false);
	const [truck, setTruck] = useState(shipInfo);
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage, userData } = context;
	const [ formAction, setFormAction ] = useState('edit');
	const [formTitle, setFormTitle] = useState('Edit Load');
	context.formAction = formAction;
	context.setFormAction = setFormAction;
	context.formTitle = formTitle;
	context.setFormTitle = setFormTitle
	context.loadState = load;
	context.setLoadState = setLoad;
	const reset = () => {
		setInfoMessage('');
	}

	useEffect(() => {
		try {
			api.getLoad(id).then((...data) => {
				if (data.message) {
					setInfoMessage(data.message)
					setTimeout(reset, 1000);
				}
				const loadData = data[0].load;
				setLoad({ ...load, ...loadData });
			})
		} catch (e) {
			
		}
	}, []);

	const handleAssignLoad = () => {
		api.assignLoads(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setTimeout(reset, 1000);
				}
				api.getLoad(id).then((...data) => {
					if (data.message) {
						setInfoMessage(data.message);
						setTimeout(reset, 1000);
					}
					const loadData = data[0].load;
					setLoad({ ...load, ...loadData });
				})
		})
	}

	const handleGetShippingInfo = (id) => {
		api.getShippingInfo(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setTimeout(reset, 1000);
					return;
				}
				const newTruck = data.truck[0];
				setTruck({ ...truck, ...newTruck });
				setInfo(true);
		})
	}

	const changeLoadState = () => {
		api.patchLoad(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setTimeout(reset, 3000);
				}
				api.getLoad(id).then((...data) => {
					if (data.message) {
						setInfoMessage(data.message);
						setTimeout(reset, 3000);
					}
					const loadData = data[0].load;
					setLoad({ ...load, ...loadData });
				})
			})
		}

	const shippingList = () => {
		const { logs } = load;
		return (
			logs.map((log, index) => {
				return (
					<Accordion key={log._id} className={classes.accordion}>
						<AccordionSummary
							expandIcon={<Icon>keyboard_arrow_down</Icon>}
							aria-controls="panel1a-content"
							id="panel1a-header"
						>
							<Typography className={classes.heading}>Log {index +1 }</Typography>
						</AccordionSummary>
						<AccordionDetails className={`${classes.accordionContent} + ${classes.content}`}>
							<Typography component="p" className={classes.cardInfo}>
									<Typography component="span">Message:</Typography>
									<Typography component="span">{log.message}</Typography>
							</Typography>
							<Typography component="p" className={classes.cardInfo}>
									<Typography component="span">Time:</Typography>
									<Typography component="span">{log.time}</Typography>
							</Typography>
						</AccordionDetails>
					</Accordion>
				)
			}) 
		)
	}


	return (
		<Box >
				<Card className={classes.card}>
				<CardContent className={classes.content}>
					<Box className={classes.cardWrapper}>
						<Box>
						<Typography component="h2" className={classes.cardTitle}>Load</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Name:</Typography>
								<Typography component="span">{load.name}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Status:</Typography>
								<Typography component="span">{load.status}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">State:</Typography>
								<Typography component="span">{load.state}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Payload:</Typography>
								<Typography component="span">{load.payload}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Delivery address:</Typography>
								<Typography component="span">{load.delivery_address}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Pickup address:</Typography>
								<Typography component="span">{load.pickup_address}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Width:</Typography>
								<Typography component="span">{load.dimensions.width}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Length:</Typography>
								<Typography component="span">{load.dimensions.length}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Height:</Typography>
								<Typography component="span">{load.dimensions.height}</Typography>
						</Typography>
					</Box>
					</Box>
					{info ?
						<Box className={classes.cardWrapper}>
						<Typography component="h2" className={classes.cardTitle}>Truck</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Status: </Typography>
								<Typography component="span">{truck.status}</Typography>
							</Typography>
							<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Type:</Typography>
								<Typography component="span">{truck.type}</Typography>
						</Typography>
					</Box> :
						null
					}
					{userData.role === 'SHIPPER' ?
						<Box className={classes.accordionBox}>
							{shippingList}
						</Box> :
						null
					}
					<Box className={classes.buttonContainer}>
						{userData.role === 'SHIPPER' ?
							<Fragment>
								<Link
									to='/account/form'
									className={`${classes.button} + ${classes.buttonLink}`}
								>
									Edit
								</Link>
								<Button
									variant="contained"
									color="primary"
									className={classes.button}
									onClick={handleAssignLoad}
								>
									Post
								</Button>
								<Button
									variant="contained"
									color="primary"
									onClick={() => handleGetShippingInfo(load._id)}
									className={classes.button}
								>
										Shipping Info
								</Button>
							</Fragment>
						:
							<Button
								variant="contained"
								color="primary"
								onClick={changeLoadState}
							>
								Change Load State
							</Button>
						}
						</Box> 
					</CardContent>
				</Card>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
				null
					}
			</Box>
	)
}

export default Load;

