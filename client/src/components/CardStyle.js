
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles({
	card: {
		width: '50%',
		margin: 'auto',
		marginTop: 50
	},
	cardTitle: {
		fontSize: 24
	},
	content: {
		width: 'auto',
		display: 'flex',
		flexWrap: 'wrap',
		alignContent: 'center',
		justifyContent: 'space-between'
	},
	accordion: {
		width: '100%',
	},
	cardWrapper: {
		width: '40%',
		marginBottom: 30
	}, 
	cardInfo: {
		display: 'flex',
		justifyContent: 'space-between'
	},
	buttonLink: {
		color: '#fff',
		backgroundColor: '#3f51b5',
		'&:hover': {
			backgroundColor: '#303f9f',
		},
		lineHeight: '1.75',
		padding: '6px 16px',
		fontSize: '0.875rem',
		textTransform: 'uppercase',
		borderRadius: '4px',
		letterSpacing: '0.02857em',
		fontWeight: 500,
		textDecoration: 'none',
		boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12);',
		transition: 'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;'
	},
	buttonContainer: {
		display: 'flex',
		width: '100%',
		marginTop: 30
	}, 
	button: {
		marginRight: 20
	},
	accordionContent: {
		'& p': {
			width: '100%'
		}
	},
	accInfo: {
		width: '100%'
	},
	field: {
		width: '80%'
	},
	form: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-between'
	},
	halfWidthContainer: {
		width: '25%'
	}
});

export default useStyles;