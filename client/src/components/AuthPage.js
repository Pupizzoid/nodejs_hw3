import React, {useState, useContext} from 'react';
import { Link, useHistory } from 'react-router-dom';
import api from '../api';
import AppContext from '../context/AppContext';
import {
	CardContent,
	Card,
	Typography,
	Button,
	TextField,
	Box
} from '@material-ui/core';
import InfoMessage from './InfoMessage';
import authStyle from './AuthStyle';
import { set } from 'mongoose';


const AuthPage = () => {
	const classes = authStyle();
	const history = useHistory();
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage, } = context;
	const [loginForm, setLoginForm] = useState({
		email: '',
		password: ''
	});
	const reset = () => {
		setInfoMessage('');
	}

	const setUserDataToStorage = (jwt_token) => {
		localStorage.setItem('token', jwt_token);
	}

	const handlerChangeInput = (e) => {
		setLoginForm({
			...loginForm,
			[e.target.name]: e.target.value 
		})
	}

	const handleSubmit = (e) => {
		e.preventDefault();
		api.login(loginForm).then((...data) => {
			if (data[0].message) {
				setInfoMessage(data[0].message);
				setTimeout(reset, 3000);
			}
			if (data[0].jwt_token) {
				setUserDataToStorage(data[0].jwt_token);
				history.push('/account');
			} else {
				setLoginForm({ email: '', password: '' });
			}
		});
	}

	return (
		<Box>
			<Card className={classes.authCard}>
				<CardContent>
					<Typography
						color='textSecondary'
						component='h1'
						className={classes.title}>
						Login
					</Typography>
					<form
						className={classes.formAuth}
						onSubmit={handleSubmit}
					>
						<TextField
							id='email'
							label='Email'
							variant='outlined'
							name='email'
							className={classes.field}
							onChange={handlerChangeInput}
							value={loginForm.email}
						/>
						<TextField
							id='password'
							label='Password'
							variant='outlined'
							placeholder='******'
							name='password'
							className={classes.field}
							onChange={handlerChangeInput}
							value={loginForm.password}
						/>
						<Button
							variant='contained'
							color='primary'
							className={classes.button}
							type='submit'
						>
							Sign in
						</Button>
					</form>
					<Link className={classes.link} to='/register'>
						Don't have an account?
					</Link>
				</CardContent>
			</Card>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
			null
		}
		</Box>
	)
}

export default AuthPage;