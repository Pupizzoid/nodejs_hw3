import { makeStyles } from '@material-ui/core/styles';

const authStyle = makeStyles({
	authCard: {
		width: 400,
		marginTop: 100,
		margin: 'auto'
  },
  title: {
		fontSize: 26,
		textAlign: 'center',
		marginBottom: 50
	},
	formAuth: {
		width: '100%',
		display: 'flex',
		flexWrap: 'wrap',
	},
	field: {
		width: '100%',
		marginBottom: '20px'
	},
	button: {
		width: '100%',
		marginBottom: '20px'
	},
	link: {
		marginBottom: '20px',
		display: 'block',
		width: '100%',
		textAlign: 'center'
	}
})
export default authStyle;