import React, { useState, useEffect } from 'react';
import { useParams}from 'react-router-dom';
import api from '../api';
// import { makeStyles } from '@material-ui/core/styles';
import useStyles from './CardStyle';
import InfoMessage from './InfoMessage';
import {
	Typography,
	Button,
	Box,
	Card,
	CardContent,
	Select,
	MenuItem 
} from '@material-ui/core';

const Truck = () => {
	const [isEdit, setIsEdit] = useState(false);
	const [truck, setTruck] = useState({
		type: '',
		status: '',
		completed: false,
		_id: ''
	});
	const [infoMessage, setInfoMessage] = useState('');
	const classes = useStyles();
	const { id } = useParams();
	const reset = () => {
		setInfoMessage('');
	}

	useEffect(() => {
		try {
			api.getTruck(id).then((...data) => {
				const newTruck = data[0].truck;
				setTruck({ ...truck, ...newTruck });
			})
		} catch (e) {
			console.log('No notes');
		}
	}, []);

	const handleEditType = () => {
		setIsEdit(true);
	}

	const handleAssignTruck = () => {
		api.assignTruck(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000);
				}
				api.getTruck(id).then((...data) => {
					const newTruck = data[0].truck;
					setTruck({ ...truck, ...newTruck });
				})
		})
}

	const handleSubmitTruckData = (e) => {
		e.preventDefault();
		if (truck.type.trim() === '') {
			return;
		}
		api.putTruck(id, { type: truck.type })
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000);
				}
			});
		setIsEdit(false);
	}


	return (
		<Box >
			<Card className={classes.card}>
			{!isEdit ?
				<CardContent className={classes.content}>
						<Box className={classes.cardWrapper}>
						<Typography component="h2" className={classes.cardTitle}>Truck</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Type:</Typography>
								<Typography component="span">{truck.type}</Typography>
						</Typography>
						<Typography component="p" className={classes.cardInfo}>
								<Typography component="span">Status:</Typography>
								<Typography component="span">{truck.status}</Typography>
						</Typography>
					</Box>
						<Box className={classes.buttonContainer}>
							<Button
								onClick={handleEditType}
								variant="contained"
								color="primary"
								className={classes.button}
							>
								Edit
							</Button>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								onClick={() => handleAssignTruck()}
							>
								Assign
							</Button>
					</Box>
				</CardContent> :
				<CardContent>
					<form
						className={classes.form}
						onSubmit={handleSubmitTruckData}
					>
						<Select
							value={truck.type}
							onChange={(e) => setTruck({ ...truck, type: e.target.value })}
							className={classes.field}
						>
							<MenuItem value='SPRINTER'>SPRINTER</MenuItem>
							<MenuItem value='SMALL_STRAIGHT'>SMALL_STRAIGHT</MenuItem>
							<MenuItem value='LARGE_STRAIGHT'>LARGE_STRAIGHT</MenuItem>
						</Select>
						<Box className={`${classes.buttonContainer} + ${classes.halfWidthContainer}`}>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								type='submit'>
								Save
							</Button>
							<Button
								variant="contained"
								color="primary"
								type='button'
								className={classes.button}
								onClick={() =>setIsEdit(false)}
							>
								Cancel
							</Button>
						</Box>
					</form>
				</CardContent>
			}
			</Card>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
				null
					}
		</Box>
	)
}

export default Truck;
