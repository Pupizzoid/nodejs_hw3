import React, { useState, useEffect, useContext} from 'react';
import { Link }from 'react-router-dom';
import api from '../api';
import useStyles from './ListStyle';
import InfoMessage from './InfoMessage';
import AppContext from '../context/AppContext';
import {
	Box,
	ListItem,
	Typography,
	Button,
	List,
	Card,
	CardContent,
	Icon,
	Select,
	MenuItem 
} from '@material-ui/core';

const TrucksList = () => {
	const [typeTruck, setTypeTruck] = useState('');
	const [truckFormState, setTruckFormState] = useState(false);
	const { infoMessage, setInfoMessage } = useContext(AppContext);
	const [dataTrucks, setDataTrucks] = useState([]);
	const classes = useStyles();
	const reset = () => {
		setInfoMessage('');
	}
	useEffect(() => {
		try {
			api.getTrucks()
				.then((...data) => {
					if (data.message) {
						setInfoMessage(data.message);
						setInterval(reset, 1000);
					}
					const newData = data[0].trucks;
					setDataTrucks([...newData]);
			})
		} catch (e) {
		}
	}, []);

	const handleSubmitTruckData = (e) => {
		e.preventDefault();
		api.postTruck({type: typeTruck})
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000);
				}
				setTruckFormState(false);
				setTypeTruck('');
				api.getTrucks()
					.then((...data) => {
						const newData = data[0].trucks;
						setDataTrucks([...newData]);
					});
		})
	}

	const handleDeleteTruck = (id) => {
		api.deleteTruck(id)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setInterval(reset, 1000);
				}
				api.getTrucks()
				.then((...data) => {
					const newData = data[0].trucks;
					setDataTrucks([...newData]);
				});
			}
		)
	}

	const drawTrucks = dataTrucks.map(truck => {
			return (
				<ListItem key={truck._id}>
					<Card className={classes.card}>
						<CardContent className={classes.content}>
						<Typography
							color="textSecondary"
							component="p"
							className={classes.text}>
							Type: {truck.type}
							</Typography>
							<Typography
								color="textSecondary"
								component="p"
								className={classes.text}>
								Status: {truck.status}
							</Typography>
							<Box className={`${classes.buttonWrapper} + ${classes.buttonContainer}`}>
								<Button>
									<Icon
										color="primary"
										onClick={() => handleDeleteTruck(truck._id)}
										className={classes.icon}>
										delete
									</Icon>
								</Button>
								<Link to={`/account/trucks/${truck._id}`}>
								<Icon
										color="primary"
									className={classes.icon}
								>
										visibility
									</Icon>
								</Link>
							</Box>
						</CardContent>
						</Card>
				</ListItem>
			)
		});

	return (
		<Box className={classes.root}>
				<Box className={classes.header}>
				<Typography
					color="textSecondary"
					component="h2"
					className={classes.title}>
				Your trucks
				</Typography>
				{truckFormState ?
					<form
						className={classes.form}
						onSubmit={handleSubmitTruckData}
					>
						<Select
							value={typeTruck}
							onChange={(e) => setTypeTruck(e.target.value)}
							className={classes.field}
						>
							<MenuItem value='SPRINTER'>SPRINTER</MenuItem>
							<MenuItem value='SMALL_STRAIGHT'>SMALL_STRAIGHT</MenuItem>
							<MenuItem value='LARGE_STRAIGHT'>LARGE_STRAIGHT</MenuItem>
						</Select>
						<Box className={classes.buttonContainer}>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								type='submit'>
								Add
							</Button>
							<Button
								variant="contained"
								color="primary"
								type='button'
								className={classes.button}
								onClick={() =>setTruckFormState(false)}
							>
								Cancel
							</Button>
						</Box>
					</form> :
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={() => setTruckFormState(true)}
					>
						Add New Truck
					</Button>
				}
				</Box>
				<Box className={classes.content}>
					{drawTrucks.length < 1 ?
						<Typography
							color="textSecondary"
							component="h4"
							className={classes.subTitle}>
							You don't have any notes!
						</Typography> :
						<List className={classes.list}>
							{drawTrucks}
						</List>
					}
			</Box>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
				null
					}
		</Box>
	)
}

export default TrucksList;
