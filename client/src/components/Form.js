import React, {useState, useContext} from 'react';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import InfoMessage from './InfoMessage';
import AppContext from '../context/AppContext';
import { useHistory} from 'react-router-dom';
import api from '../api';

const useStyles = makeStyles({
  root: {
		display: 'flex',
		flexWrap: 'wrap',
		maxWidth: 500,
		justifyContent: 'space-between',
		padding: 20,
		margin: 'auto'
	},
	box: {
		margin: 'auto'
	},
	TextField: {
		width: '100%',
		marginBottom: '20px'
	},
	TextFieldHalfWidth: {
		width: '33%',
		marginBottom: '20px'
	},
	title: {
		textAlign: 'center',
		fontSize: 24
	}
});

const Form = () => {
	const context = useContext(AppContext);
	const { infoMessage, setInfoMessage, formTitle, formAction, loadState } = context;
	const [loadData, setLoadData] = useState(loadState);
	const classes = useStyles();
	const reset = () => {
		setInfoMessage('');
	}
	const history = useHistory();

	
	const handleSubmit = (e) => {
		e.preventDefault();
		if (formAction === 'edit') {
			api.putLoad(loadData._id, loadData)
				.then(data => {
					if (data.message) {
						setInfoMessage(data.message);
						setTimeout(reset, 1000);
					}
					history.push(`/account/loads/${loadData._id}`);
			})
		} else {
			api.postLoads(loadData)
			.then(data => {
				if (data.message) {
					setInfoMessage(data.message);
					setTimeout(reset, 1000);
				}
				history.push('/account/loads');
		})
		}
	}

	const handleCancel = () => {
		if (formAction === 'edit') {
			history.push(`/account/loads/${loadData._id}`);
		} else {
			history.push('/account/loads');
		}
	}


	return (
		<Box className={classes.box}>
			<Typography className={classes.title} color="primary" component="h1">
				{formTitle}
			</Typography>
			<form onSubmit={handleSubmit} className={classes.root}>
					<TextField
          required
          id="name"
					label="Name"
					variant="outlined"
					className={classes.TextField}
          defaultValue={loadData.name}
					onChange={e => setLoadData({...loadData, name: e.target.value})}
        />
				<TextField
					required
					id="payload"
					label="Payload"
					variant="outlined"
					defaultValue={loadData.payload}
					className={classes.TextField}
					onChange={e => setLoadData({...loadData, payload: parseInt(e.target.value)})}
				/>
					<TextField
          required
          id="pickup_address"
					label="Pickup address"
					variant="outlined"
					className={classes.TextField}
          defaultValue={loadData.pickup_address}
					onChange={e => setLoadData({...loadData, pickup_address: e.target.value})}
        />
					<TextField
					required
					id="delivery_address"
					label="Delivery address"
					variant="outlined"
					className={classes.TextField}
					defaultValue={loadData.delivery_address}
					onChange={e => setLoadData({...loadData, delivery_address: e.target.value})}
				/>
					<TextField
					required
					id="width"
					label="Width"
					variant="outlined"
					className={classes.TextFieldHalfWidth}
					defaultValue={loadData.dimensions.width}
					onChange={e => 
						setLoadData({
							...loadData,
							dimensions: Object.assign({}, loadData.dimensions, {width: parseInt(e.target.value)})
						})
					}
					/>
					<TextField
					required
					id="length"
					label="Length"
					variant="outlined"
					className={classes.TextFieldHalfWidth}
					defaultValue={loadData.dimensions.length}
					onChange={e =>
						setLoadData({
							...loadData,
							dimensions: Object.assign({}, loadData.dimensions, { length: parseInt(e.target.value) })
						})
					}
				/>
				<TextField
					required
					id="height"
					label="Height"
					variant="outlined"
					className={classes.TextFieldHalfWidth}
					defaultValue={loadData.dimensions.height}
					onChange={e =>
						setLoadData({
							...loadData,
							dimensions: Object.assign({}, loadData.dimensions, { height: parseInt(e.target.value)})
						})
					}
					/>
					<ButtonGroup color="primary" aria-label="contained primary button group">
						<Button type='submit'>Save</Button>
					<Button
						type='button'
						onClick={handleCancel}
					>
						Cancel
					</Button>
					</ButtonGroup> 
			</form>
			{infoMessage ?
				<InfoMessage info={infoMessage} /> :
				null
		}
		</Box>
	)
}

export default Form;