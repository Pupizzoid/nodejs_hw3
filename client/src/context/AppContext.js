import { createContext } from 'react';

const AppContext = createContext({
	userData: {
		email: '',
		role: ''
	},
	setUserData: () => { },
	loadState: '',
	setLoadState: () => { },
	formTitle: '',
	setFormTitle: () => { },
	formAction: '',
	setFormAction: () => {},
	infoMessage: '',
	setInfoMessage: () => {}
});

export default AppContext;