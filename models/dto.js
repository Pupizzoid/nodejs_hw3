/* eslint-disable camelcase */
const dto = {
  user: (data) => {
    return {
      user: {
        _id: data._id,
        email: data.email,
        created_date: data.created_date,
        role: data.role,
      },
    };
  },
  truck: (data) => {
    if (!Array.isArray(data)) {
      return {
        _id: data.id,
        created_by: data.created_by,
        assigned_to: data.assigned_to,
        type: data.type,
        status: data.status,
        created_date: data.created_date,
      };
    }
    return data.map((truck) => {
      return {
        _id: truck.id,
        created_by: truck.created_by,
        assigned_to: truck.assigned_to,
        type: truck.type,
        status: truck.status,
        created_date: truck.created_date,
      };
    });
  },
  load: (data) => {
    if (!Array.isArray(data)) {
      return {
        _id: data.id,
        created_by: data.created_by,
        assigned_to: data.assigned_to,
        status: data.status,
        state: data.state,
        name: data.name,
        payload: data.payload,
        pickup_address: data.pickup_address,
        delivery_address: data.delivery_address,
        dimensions: data.dimensions,
        logs: data.logs,
        created_date: data.created_date,
      };
    }
    return data.map((load) => {
      return {
        _id: load.id,
        created_by: load.created_by,
        assigned_to: load.assigned_to,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: load.dimensions,
        logs: load.logs,
        created_date: load.created_date,
      };
    });
  },
  shippedInfo: (load, truck) => {
    return {
      load: dto.load(load),
      truck: dto.truck(truck),
    };
  },
};

module.exports = dto;
