/* eslint-disable camelcase */
const { Schema, model } = require('mongoose');

const schema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  logs: [{ message: String, time: Date }],
  payload: {
    type: Number,
    required: true,
  },
  dimensions: { width: Number, height: Number, length: Number },
});

module.exports = model('Load', schema);
