/* eslint-disable camelcase */
const { Schema, model } = require('mongoose');

const schema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

module.exports = model('Truck', schema);
