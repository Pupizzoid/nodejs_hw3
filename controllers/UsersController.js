const db = require('../abstract');
const dto = require('../models/dto');
const { handleError, check } = require('./utils');
const bcrypt = require('bcrypt');

const handlePatchUsers = async (req, res) => {
  try {
    const { newPassword, oldPassword } = req.body;
    if (await check.isPermissionsToUpdate(req.user.id)) {
      return handleError({ message: 'You can\'t do anything, when you On Load' }, res);
    }
    const user = await db.findUserById(req.user.id);
    const isMatch = await bcrypt.compare(oldPassword, user.password);
    if (!isMatch) {
      return handleError({ message: 'Invalid password' }, res);
    }
    const hashedPassword = await bcrypt.hash(newPassword, 12);
    await db.changeUserPassword(req.user.id, { password: hashedPassword }, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Password changed successfully' });
    });
    handleError({ message: 'Incorrect password' }, res);
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleGetUsers = async (req, res) => {
  try {
    await db.findUserById(req.user.id, (err, data) => {
      if (err) return handleError({ message: 'User not found' }, res);
      return res.status(200).json(dto.user(data));
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};


const handleDeleteUsers = async (req, res) => {
  try {
    const id = req.user.id;
    if (await check.isPermissionsToUpdate(req.user.id)) {
      return handleError({ message: 'You can\'t do anything, when you On Load' }, res);
    }
    await db.deleteUserById(id, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Profile deleted successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

module.exports = {
  handlePatchUsers,
  handleGetUsers,
  handleDeleteUsers,
};
