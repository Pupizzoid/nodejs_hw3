const jwt = require('jsonwebtoken');
const config = require('config');
const SECRET = config.get('jwtSecret');
const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const db = require('../abstract');
const {
  handleError,
} = require('./utils');

const handleAuthRegister =async (req, res) => {
  try {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}
    const { email, password } = req.body;
    const candidate = await db.findUserByEmail(email);
    if (candidate) {
      return handleError({ message: `Such user already exists` }, res);
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    await db.createUser({ ...req.body, password: hashedPassword });
    return res.status(200).json({ message: 'Profile created successfully' });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleAuthLogin = async (req, res) => {
  try {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}
    const { email, password } = req.body;
    const user = await db.findUserByEmail(email);
    if (!user) {
      return handleError({ message: `User with this email not found` }, res);
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return handleError({ message: `Invalid password` }, res);
    }
    // eslint-disable-next-line camelcase
    return res.status(200).json({ jwt_token: jwt.sign(({ id: user.id }), SECRET) } );
  } catch (e) {
    handleError(e, res, 500);
  }
};

module.exports = {
  handleAuthRegister,
  handleAuthLogin,
};
