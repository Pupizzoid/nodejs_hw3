/* eslint-disable camelcase */
const db = require('../abstract');
const dto = require('../models/dto');
const { shipper, handleError, getUserRoleBy, check } = require('./utils');

const handleGetTrucks = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    await db.findUsersTrucks(req.user.id, (err, data) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ trucks: dto.truck(data) });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};
const handleCreateTruck = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    const { type } = req.body;
    const truck = await db.createTruck(type, req.user.id);
    if (!truck) {
      return handleError({ message: 'Truck wasn\'t created' }, res);
    }
    return res.status(200).json({ message: 'Truck created successfully' });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleGetTruckById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    const { id } = req.params;
    await db.findTruckById(id, (err, data) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ truck: dto.truck(data) });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleUpdateTruckById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    const { type } = req.body;
    if (!check.isPermissionsToUpdate(req.user.id, 'OL')) {
      return handleError({ message: 'You can\'t change status now' }, res);
    }
    await db.updateTruckById(req.params.id, { type }, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Truck details changed successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleDeleteTruckById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    const { id } = req.params;
    await db.deleteTruckById(id, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Truck deleted successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleAssignTruckByUser = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    // !! ToDo check all permissions for driver when he has load
    const { id } = req.params;
    if (await check.assignedTruck(req.user.id)) {
      return handleError({ message: 'You already have assigned truck' }, res);
    }
    await db.updateTruckById(id, { assigned_to: req.user.id, status: 'IS' }, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Truck assigned successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};


module.exports = {
  handleGetTrucks,
  handleCreateTruck,
  handleGetTruckById,
  handleUpdateTruckById,
  handleDeleteTruckById,
  handleAssignTruckByUser,
};
