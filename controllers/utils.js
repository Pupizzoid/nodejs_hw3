/* eslint-disable no-prototype-builtins */
const db = require('../abstract');

const driver = 'DRIVER';
const shipper = 'SHIPPER';

const loadState = ['Out of service', 'En route to Pick Up', 'Arrived to Pick Up', 'El route to delivery', 'Arrived to delivery'];

const truckType = {
  SPRINTER: {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  SMALL_STRAIGHT: {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  LARGE_STRAIGHT: {
    width: 700,
    length: 250,
    height: 200,
    payload: 4000,
  },
};

const defineRightTruckType = (width, length, height, payload) => {
  const result = {};
  for (const key in truckType) {
    if (!truckType.hasOwnProperty(key)) {
      continue;
    }
    const truck = truckType[key];
    if (check.isLoadMatching(truck, { width, length, height, payload })) {
      result[key] = Object.assign({}, truck);
    }
  }
  return Object.keys(result).map((truck) => ({
    type: truck,
    status: 'IS',
  }));
};

const handleWriteLogs = (message) => {
  const time = new Date().toLocaleString();
  const log = {
    message: message,
    time: time,
  };
  return log;
};

const getUserRoleBy = async (id) => {
  return (await db.findUserById(id)).role;
};

const getLoadStatusBy = async (id) => {
  return (await db.findLoadById(id)).status;
};

const check = {
  assignedTruck: async (id) => {
    const truckList = await db.findAssignedUserTruck(id);
    return truckList.length > 0;
  },
  isLoadMatching: (truck, load) => {
    return truck.width > load.width &&
            truck.height > load.height &&
            truck.length > load.length &&
            truck.payload > load.payload;
  },
  isPermissionsToUpdate: async (id) => {
    const isPermission = await db.findAssignedUserTruck(id);
    return isPermission.length !== 0 && isPermission[0].status === 'OL';
  },
  activeLoad: (load) => {
    return load.status !== 'POSTED' && load.status !== 'NEW';
  },
};

const defaultErrHandler = (err, res) => {
  if (err) return handleError(err, res);
};

const getNewLoadState = (state) => {
  const stateIndex = loadState.indexOf(state);
  if (stateIndex === loadState.length - 1) {
    return false;
  }
  return loadState[stateIndex + 1];
};

const handleError = (err, res, errCode = 400) => {
  return res.status(errCode).json({ message: err.message });
};


module.exports = {
  check,
  truckType,
  driver,
  shipper,
  loadState,
  getNewLoadState,
  defineRightTruckType,
  handleError,
  defaultErrHandler,
  getUserRoleBy,
  getLoadStatusBy,
  handleWriteLogs,
};
