/* eslint-disable camelcase */
const db = require('../abstract');
const dto = require('../models/dto');
const {
  getNewLoadState,
  defineRightTruckType,
  shipper,
  driver,
  getUserRoleBy,
  loadState,
  handleError,
  defaultErrHandler,
  getLoadStatusBy,
  handleWriteLogs,
} = require('./utils');

const handleGetLoadsForDriver = (req, res) => {
  return db.findLoadsForDriver(req.user.id, (err, data) => {
    if (err) return handleError(err, res);
    return res.status(200).json({ loads: dto.load(data) });
  });
};

const handleGetLoadsForShipper = (req, res, params) => {
  return db.findLoads(params, (err, data) => {
    if (err) return handleError(err, res);
    return res.status(200).json({ loads: dto.load(data) });
  });
};

const handleGetLoads = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return await handleGetLoadsForDriver(req, res);
    }
    if (req.query && req.query.status) {
      return await handleGetLoadsForShipper(req, res, { created_by: req.user.id, status: req.query.status.toUpperCase() });
    }
    await handleGetLoadsForShipper(req, res, { created_by: req.user.id });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleCreateLoads = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const load = await db.createLoad({ name, payload, pickup_address, delivery_address, dimensions, created_by: req.user.id, state: loadState[0] });
    if (!load) {
      return handleError({ message: 'Truck wasn\'t created' }, res);
    }
    return res.status(200).json({ message: 'Load created successfully' });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleGetActiveLoads = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    await db.findActiveLoadForDriver(req.user.id, (err, data) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ load: dto.load(data) });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleUpdateDeliveryStatus = async (req, res, load, state) => {
  await db.findAssignedUserTruck(req.user.id, async (err, truck) => {
    if (err) return handleError(err, res);
    await db.updateTruckById(truck[0]._id, { status: 'IS' }, defaultErrHandler);
  });
  const log = handleWriteLogs(`Load state changed to "${state}"`);
  return await db.updateLoadByParams(load._id, { status: 'SHIPPED', state, $push: { logs: log } }, (err) => {
    if (err) return defaultErrHandler(err, res);
    return res.status(200).json({ message: `Load state changed to "${state}"` });
  });
};

const handleUpdateActiveLoads = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === shipper) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const load = (await db.findActiveLoadForDriver(req.user.id, defaultErrHandler))[0];
    if (!load) {
      return handleError({ message: 'Already shipped' }, res);
    }
    const state = getNewLoadState(load.state);
    if (!state) {
      return handleError({ message: 'Already shipped' }, res);
    }
    if (state === loadState[4]) {
      return handleUpdateDeliveryStatus(req, res, load, state);
    }
    const log = handleWriteLogs(`Load state changed to "${state}"`);
    await db.updateLoadByParams(load._id, { state, $push: { logs: log } }, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: `Load state changed to "${state}"` });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};


const handleGetLoadById = async (req, res) => {
  try {
    await db.findLoadById(req.params.id, (err, data) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ load: dto.load(data) });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleUpdateLoadById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const loadStatus = await getLoadStatusBy(req.params.id);
    if (loadStatus !== 'NEW') {
      return handleError({ message: 'You can\'t change load' }, res);
    }
    await db.updateLoadByParams(req.params.id, { ...req.body }, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Load details changed successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const handleDeleteLoadById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const loadStatus = await getLoadStatusBy(req.params.id);
    if (loadStatus !== 'NEW') {
      return handleError({ message: 'You can\'t delete load' }, res);
    }
    const { id } = req.params;
    await db.deleteLoadById(id, (err) => {
      if (err) return handleError(err, res);
      return res.status(200).json({ message: 'Load deleted successfully' });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

const changeLoadStatus = (id, res, status) => {
  const log = handleWriteLogs(`Load was changed status on ${status}`);
  return db.updateLoadByParams(id, {
    status,
    $push: { logs: log },
  }, async (err) => {
    if (err) return handleError(err, res);
  });
};
const handlePostLoad = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const { id } = req.params;
    // TODO removed await here due to duplication problem of logs, check if this leads to any problems
    changeLoadStatus(id, res, 'POSTED');
    const load = await db.findLoadById(id);
    const { width, length, height } = load.dimensions;
    const suitableTypesOfTrucks = await defineRightTruckType(width, length, height, load.payload);
    if (suitableTypesOfTrucks.length === 0) {
      changeLoadStatus(id, res, 'NEW');
      return handleError({ message: 'Don\'t any truck for you load' }, res);
    }
    const truckList = await db.findTruck(suitableTypesOfTrucks);
    if (truckList.length === 0) {
      changeLoadStatus(id, res, 'NEW');
      return handleError({ message: 'Don\'t have any free truck' }, res);
    }
    const { _id, assigned_to } = truckList[0];
    await db.updateTruckById(_id, { status: 'OL' }, defaultErrHandler);
    const log = handleWriteLogs(`Load was assigned for driver with id#${assigned_to}`);
    db.updateLoadByParams(id, { status: 'ASSIGNED', assigned_to, state: getNewLoadState(load.state), $push: {
      logs: log,
    } }, defaultErrHandler);
    return res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};


const handleGetActiveLoadById = async (req, res) => {
  try {
    const userRole = await getUserRoleBy(req.user.id);
    if (userRole === driver) {
      return handleError({ message: 'You don\'t have permissions' }, res);
    }
    const { id } = req.params;
    await db.findLoadById(id, async (err, data) => {
      if (err) return handleError(err, res);
      if (data.status === 'NEW' || data.status === 'POSTED') {
        return handleError({ message: `It's load not active` }, res);
      }
      const truck = await db.findAssignedUserTruck(data.assigned_to);
      return res.status(200).json({ load: dto.load(data), truck: dto.truck(truck) });
    });
  } catch (e) {
    handleError(e, res, 500);
  }
};

module.exports = {
  handleGetLoads,
  handleCreateLoads,
  handleGetActiveLoads,
  handleUpdateActiveLoads,
  handleGetLoadById,
  handleUpdateLoadById,
  handleDeleteLoadById,
  handlePostLoad,
  handleGetActiveLoadById,
};
